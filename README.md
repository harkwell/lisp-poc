Khallware (Lisp Proofs-of-Concept)
=================
Overview
---------------
Here are some simple proofs-of-concept written in lisp (scheme).

The "/usr/bin/guile" program is an implementation of scheme which is a popular
variant of the lisp (LISt Processor) programming language.

Installation
---------------
```shell
docker run -it -h scheme --name scheme centos bash
yum install -y guile
cat <<'EOF' >main.scm
#!/usr/bin/guile -s
!#
(define name "main")
(display "Hello World!\n")
EOF
chmod 777 main.scm
./main.scm
```
