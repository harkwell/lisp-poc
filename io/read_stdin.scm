#!/usr/bin/guile -s
!#

(define name "read_stdin")
(use-modules (ice-9 rdelim))
(define user_input (read-line))
(display (string-append user_input "\n"))
