#!/usr/bin/guile -s
!#
; sed -in -e 's#IPADDR#'$(grep scheme /etc/hosts |awk '{print $1}')'#' main.scm
; ./main.scm
; IPADDR=$(docker inspect scheme |grep IPAddress |head -1 |cut -d\"  -f4)
; curl http://$IPADDR/ # from outside of docker

(use-modules (web server))
(define (handler request request-body)
	(values '((content-type . (text/plain)))
		"Hello World"))

(run-server handler 'http ' (#:host "IPADDR" #:port 80))
