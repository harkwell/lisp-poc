#!/usr/bin/guile -s
!#

(use-modules (ice-9 rdelim))
(define child #f)

(let
	((s (socket PF_INET SOCK_STREAM 0)))
	(setsockopt s SOL_SOCKET SO_REUSEADDR 1)
	(bind s AF_INET INADDR_ANY 7)
	(listen s 5)

	(while #t (let*
		((client-connection (accept s))
		(client-details (cdr client-connection))
		(client (car client-connection)))
		(set! child (primitive-fork))

		(if (zero? child) (begin
			(set! child (primitive-fork))

			(if (zero? child) (begin (do
				((line (read-line client)(read-line client)))
				((zero? 1))
				(display line client)
				(newline client)
			)))
			(primitive-exit)
		) (waitpid child))
	))
)
